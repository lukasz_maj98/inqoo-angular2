import { Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Playlists} from '../../model/playlists';
@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss']
})
export class PlaylistsListComponent implements OnInit {

selectedId: any = '123';

  @Input() playlists: Playlists []= []
  @Input() selected?: Playlists

  @Output()selectedChange = new EventEmitter<Playlists>()

  constructor() { }
    
  ngOnInit(): void {
  }

  select(playlist: Playlists) {
    this.selectedChange.emit(playlist)
  }
}