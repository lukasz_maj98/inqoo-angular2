
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Playlists } from '../../model/playlists';

@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.scss']
})
export class PlaylistEditorComponent implements OnInit {


  @Input() playlists!: Playlists

  @Output() cancel = new EventEmitter()
  @Output() save = new EventEmitter<Playlists>()

  draft!: Playlists
 
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.draft = {...this.playlists}
  }

  childCancel(playlist: Playlists) {
    this.cancel.emit(playlist)
  }

  childSaveEdit(draft: Playlists) {
    this.save.emit(draft)
  }
  
}

