import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlists } from '../../model/playlists';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() playlists!: Playlists

  @Output() edit = new EventEmitter()
  @Output() create = new EventEmitter<Playlists>()


  
  constructor() { }

  ngOnInit(): void {
  }

  childEdit(playlist: Playlists) {
    this.edit.emit(playlist)
  }
  
  childCreate() {
    this.create.emit()
  }

}



