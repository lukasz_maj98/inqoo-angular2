import { Component, OnInit } from '@angular/core';
import { Playlists } from '../../model/playlists';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  mode: "details" | "edit" | "create" = 'details'

  playlists: Playlists []= [
    {
        id: '123',
        name: ' Playlist 123',
        public: true,
        description: 'my favourite playlist'
      }, {
        id: '234',
        name: ' Playlist 234',
        public: true,
        description: 'my favourite playlist'
      }, {
        id: '345',
        name: ' Playlist 345',
        public: false,
        description: 'my favourite playlist'
      }]


      selected = this.playlists[0]


  constructor() { }

  edit() { 
    this.mode ="edit"
  }
  
  cancel() { 
    this.mode ="details"
  }

  selectPlaylist(playlist: Playlists) {
    this.selected = playlist
  }

  save(draft: Playlists) {
    this.mode = "details"
    this.playlists.push(draft)
    
  }

  ngOnInit(): void {
  }

}
