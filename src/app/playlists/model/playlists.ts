export interface Playlists{
    id: string;
    name: string;
    public: boolean;
    description: string;
}