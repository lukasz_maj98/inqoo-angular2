import { Component, Inject, OnInit } from '@angular/core';
import { Album } from 'src/app/core/model/album';
import { AlbumSearchService } from 'src/app/core/services/album-search/album-search.service';

@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.scss']
})
export class AlbumSearchComponent implements OnInit {
  results: Album[] = []

  constructor(
    private service: AlbumSearchService
  ) { }

  search(query: string) {
    this.service.getAlbums(query)
      .subscribe({
        next: (resp) => {
          console.log('next', resp)
          this.results = resp.albums.items
        },
        error: (error) => {
          console.log('error', error)
        },
        // complete: () => {
        //   console.log('complete')
        // },
      })
  }

  ngOnInit(): void {
    // this.results = this.service.getAlbums()
  }

}
